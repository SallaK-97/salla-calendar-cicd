import express, { Request, Response } from 'express';

interface Event {
  id: string;
  title: string;
  description: string;
  date: string;
  time?: string;
}

const app = express();
app.use(express.json());

let events: Event[] = [];

app.get('/', (_req: Request, res: Response) => {
  res.json(events);
  res.send("muutos4");
});

app.get('/version', (req: Request, res: Response) => {
  res.send('1.0')
})

app.get('/:monthNumber', (req: Request, res: Response) => {
  const monthNumber = parseInt(req.params.monthNumber, 10);

  const eventsInSpecifiedMonth = events.filter(event => {
    const eventDate = new Date(event.date);
    const eventMonth = eventDate.getMonth();
    return eventMonth + 1 === monthNumber;
  });

  res.json(eventsInSpecifiedMonth);
});

app.post('/', (req: Request, res: Response) => {
  const event: Event = req.body;
  events.push(event);
  res.status(201).json(event);
});

app.put('/:eventId', (req: Request, res: Response) => {
  const eventId = req.params.eventId;
  const updatedEvent: Event = req.body;

  events = events.map(event => {
    if (event.id === eventId) {
      return { ...event, ...updatedEvent };
    }
    return event;
  });

  res.sendStatus(204);
});

app.delete('/:eventId', (req: Request, res: Response) => {
  const eventId = req.params.eventId;
  events = events.filter(event => event.id !== eventId);
  res.sendStatus(204);
});



const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log('Calendar is running on port', port);
});

